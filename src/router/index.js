import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    redirect:'/login'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },
  {
    path:'/index',
    name:'index',
    component: () => import(/* webpackChunkName: "index" */ '../views/Index.vue'),
    children:[
      {
        path:'users',
        name:'users',
        component: () => import(/* webpackChunkName: "users" */ '../views/users/Users.vue'),
      },
      {
        path:'roles',
        name:'roles',
        component: () => import(/* webpackChunkName: "roles" */ '../views/roles/Roles.vue'),
      },
      {
        path:'rights',
        name:'rights',
        component: () => import(/* webpackChunkName: "rights" */ '../views/rights/Rights.vue'),
      },
      {
        path:'goods',
        name:'goods',
        component: () => import(/* webpackChunkName: "goods" */ '../views/goods/Goods.vue'),
      },
      {
        path:'params',
        name:'params',
        component: () => import(/* webpackChunkName: "params" */ '../views/params/Params.vue'),
      },
      {
        path:'categories',
        name:'categories',
        component: () => import(/* webpackChunkName: "categories" */ '../views/categories/Categories.vue'),
      },
      {
        path:'orders',
        name:'orders',
        component: () => import(/* webpackChunkName: "orders" */ '../views/orders/Orders.vue'),
      },
      {
        path:'reports',
        name:'reports',
        component: () => import(/* webpackChunkName: "reports" */ '../views/reports/Reports.vue'),
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
