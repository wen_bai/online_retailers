import axios from 'axios'
const instance=axios.create({
    baseURL:'http://localhost:8888/api/private/v1/'
})
import {Message} from "element-ui"
import  store from '../store'
import router from '../router'
// 请求拦截器
instance.interceptors.request.use((config)=>{
    if(store.state.token){
        config.headers['Authorization']=store.state.token
    }
    // else{
    //     router.push({
    //         name:'login'
    //     })
    // }
    return config
},error=>{
        return Promise.reject(error)
})
// // 相应拦截器
instance.interceptors.response.use((response)=>{
    if(response.data.meta.status===400){
        router.push({
            name:'login'
        })
    }
    return response
},(error)=>{
    return Promise.reject(error)
})
function http(url,method,data,params){
    return new Promise((resolve,reject)=>{
        instance({
            url,
            method,
            data,
            params
        })
        .then(res=>{
            if((res.status>=200&&res.status<300)||res.status===304){
                if((res.data.meta.status>=200&&res.data.meta.status<300)||res.data.meta.status===304){
                    resolve(res.data)
                }else{
                    Message({
                        showClose:true,
                        message:res.data.meta.msg,
                        type:'erroe'
                    })
                    reject(res)
                }
            }else{
                Message({
                showClose:true,
                message:res.statusText,
                type:'erroe'
            })
                reject(res)
            }
        }).catch(err=>{
            Message({
                showClose:true,
                message:err.message,
                type:'erroe'
            })
            reject(err)
        })
    })
}

export  {
    http
}