import {http} from './index'
import { Message } from 'element-ui'

//登陆方法
export function login(data){
    return http("login", "POST",data)
}
// 获取权限列表
export function getMenus(){
    return http ("menus","GET")
}
// 获取用户列表
export function getUsers(params){
    return http ("users","GET",{},params)
}
//添加用户
export function addUsers(data){
    return http ("users","POST",data)
}
//改变用户状态
export function stateUser(uId,type,data){
    return http ("users/"+uId+"/state/"+type,'PUT',data)
}
//编辑用户提交
export function editUser(id,data){
    return http (`users/${id}`,'PUT',data)
}
//删除角色
export function deleteUser(id,data){
    return http (`users/${id}`,'delete',data).then(res=>{
        Message({
            showClose:true,
            message:res.meta.msg,
            type:"success"
        })
        return res.data
    })
}
// 分配用户角色
export function deliverRole(id,rid){
    return http (`users/${id}/role`,'PUT',rid)
    // .then(res=>{
    //     // Message({
    //     //     showClose:true,
    //     //     message:res.meta.msg,
    //     //     type:"success"
    //     // })
    //     // return res.data
    // })
}
export function getRoles(){
    return http ("roles","GET")
}